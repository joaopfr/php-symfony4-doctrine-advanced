<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190125170029 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE time (id INT AUTO_INCREMENT NOT NULL, nome VARCHAR(50) NOT NULL, escudo VARCHAR(20) NOT NULL, ativo TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE partida (id INT AUTO_INCREMENT NOT NULL, campeonato_id INT DEFAULT NULL, time_casa_id INT DEFAULT NULL, time_visitante_id INT DEFAULT NULL, descricao LONGTEXT NOT NULL, placar_visitante INT NOT NULL, placar_casa INT NOT NULL, data_partida DATETIME NOT NULL, INDEX IDX_A9C1580C93BAAE11 (campeonato_id), INDEX IDX_A9C1580C14F5E2C4 (time_casa_id), INDEX IDX_A9C1580C102C177 (time_visitante_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organizacao (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, nome VARCHAR(50) NOT NULL, INDEX IDX_51A4988B727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE campeonato (id INT AUTO_INCREMENT NOT NULL, organizacao_id INT DEFAULT NULL, nome VARCHAR(50) NOT NULL, INDEX IDX_722DB8CAE30256E3 (organizacao_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE campeonato_time (campeonato_id INT NOT NULL, time_id INT NOT NULL, INDEX IDX_644EB09693BAAE11 (campeonato_id), INDEX IDX_644EB0965EEADD3B (time_id), PRIMARY KEY(campeonato_id, time_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE partida ADD CONSTRAINT FK_A9C1580C93BAAE11 FOREIGN KEY (campeonato_id) REFERENCES campeonato (id)');
        $this->addSql('ALTER TABLE partida ADD CONSTRAINT FK_A9C1580C14F5E2C4 FOREIGN KEY (time_casa_id) REFERENCES time (id)');
        $this->addSql('ALTER TABLE partida ADD CONSTRAINT FK_A9C1580C102C177 FOREIGN KEY (time_visitante_id) REFERENCES time (id)');
        $this->addSql('ALTER TABLE organizacao ADD CONSTRAINT FK_51A4988B727ACA70 FOREIGN KEY (parent_id) REFERENCES organizacao (id)');
        $this->addSql('ALTER TABLE campeonato ADD CONSTRAINT FK_722DB8CAE30256E3 FOREIGN KEY (organizacao_id) REFERENCES organizacao (id)');
        $this->addSql('ALTER TABLE campeonato_time ADD CONSTRAINT FK_644EB09693BAAE11 FOREIGN KEY (campeonato_id) REFERENCES campeonato (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE campeonato_time ADD CONSTRAINT FK_644EB0965EEADD3B FOREIGN KEY (time_id) REFERENCES time (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE partida DROP FOREIGN KEY FK_A9C1580C14F5E2C4');
        $this->addSql('ALTER TABLE partida DROP FOREIGN KEY FK_A9C1580C102C177');
        $this->addSql('ALTER TABLE campeonato_time DROP FOREIGN KEY FK_644EB0965EEADD3B');
        $this->addSql('ALTER TABLE organizacao DROP FOREIGN KEY FK_51A4988B727ACA70');
        $this->addSql('ALTER TABLE campeonato DROP FOREIGN KEY FK_722DB8CAE30256E3');
        $this->addSql('ALTER TABLE partida DROP FOREIGN KEY FK_A9C1580C93BAAE11');
        $this->addSql('ALTER TABLE campeonato_time DROP FOREIGN KEY FK_644EB09693BAAE11');
        $this->addSql('DROP TABLE time');
        $this->addSql('DROP TABLE partida');
        $this->addSql('DROP TABLE organizacao');
        $this->addSql('DROP TABLE campeonato');
        $this->addSql('DROP TABLE campeonato_time');
    }
}
