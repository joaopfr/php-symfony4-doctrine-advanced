<?php

namespace App\Controller;

use App\Entity\Time;
use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class TimeController extends AbstractController
{
    /**
     * @Route("/time", name="listar-times")
     * @Template("time/index.html.twig")
     */
    public function index()
    {
        $timeRepo = $this->getDoctrine()->getManager()->getRepository(Time::class);
        return [
            "times" =>$timeRepo->findAll(PDO::FETCH_OBJ)
        ];
    }
}
