<?php

namespace App\Controller;

use App\Entity\Campeonato;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CampeonatoController extends AbstractController
{
    /**
     * @Route("/campeonato", name="listar-campeonatos")
     * @Template("campeonato/index.html.twig")
     */
    public function index()
    {
        $campeonatoRepo = $this->getDoctrine()->getManager()->getRepository(Campeonato::class);

        return [
            "campeonatos" => $campeonatoRepo->getCampeonatos()
        ];
    }

    /**
     * @Route("/campeonato/classificacao/{id}", name="classificacao")
     * @Template("campeonato/classificacao.html.twig")
     */
    public function classificao(Campeonato $campeonato)
    {
        $classificacao = $this->getDoctrine()
            ->getManager()
            ->getRepository(Campeonato::class)
            ->getClassificacao($campeonato);

        return [
            "campeonato" => $campeonato,
            "classificacao" => $classificacao
        ];
    }
}
