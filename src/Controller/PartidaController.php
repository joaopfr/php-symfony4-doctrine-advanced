<?php

namespace App\Controller;

use App\Entity\Partida;
use App\Entity\Time;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\VarDumper\VarDumper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PartidaController extends AbstractController
{
    /**
     * @Route("/partida", name="partida")
     */
    public function index()
    {
        $partidas = $this->getDoctrine()->getManager()->getRepository(Partida::class)->getPartidasPordata();
        VarDumper::dump($partidas);

        exit;
    }

    /**
     * @param Time $time
     * @return array
     * @Route("/partida/listar-por-time/{id}", name="listar-partida-por-time")
     * @Template("partida/listar-por-time.html.twig")
     */
    public function partidasPorTime(Time $time)
    {
        $partidasPorTime = $this->getDoctrine()
            ->getManager()
            ->getRepository(Partida::class)
            ->getPartidasPorTime($time);

        VarDumper::dump($partidasPorTime);
        return [
            "partidas" => $partidasPorTime,
            "time" => $time
        ];
    }
}
